        function runMediaQueryCode(){
            // media query event handler
            if (matchMedia) {
                var mq = window.matchMedia("(max-width: 44.5em)");
                mq.addListener(WidthChange);
                WidthChange(mq);
            }

            // media query change
            function WidthChange(mq) {
                if (mq.matches) {
                  $("#showSettings").addClass("showSettings");
                  $(".showSettings").on("click",function(){
                    $(".test-wrapper__settings").toggle(500);
                  });
                }
                else {
                  $(".showSettings").unbind("click");
                  $("#showSettings").removeClass("showSettings");
                  $(".test-wrapper__settings").show();
                }
            }
        }

        $(document).ready(function() {
            runMediaQueryCode();
            $(".ResultClearElements").hide();
            $(".ResultChangeElements").hide();

            $("#selectCssOption").on("change",function(){
                var selectedOption = $("#selectCssOption").val();

                $.ajax({
                    url: "/set-css-option",
                    type: "POST",
                    data: { cssOption: selectedOption },
                    success: function(response){

                    },
                    error: function(response){
                        console.log(response);
                    }
                });
            });


            $("#btnResult").on("click", function() {
                var MyWindow = window.open("/run-loading-performance-test", "_blank");
            });

            $("#btnClear").on("click",function(){
                $.ajax({
                    url: "/clear-loading-performance-result",
                    type: "POST",
                    success: function(response){
                        $(".ResultClearElements").html("<strong>Results cleared</strong>");
                        $(".ResultClearElements").show();
                            setTimeout(function() {
                                  $(".ResultClearElements").hide(1000);
                        }, 3000);
                        $(".test-result-wrapper").hide(1000);
                    },
                    error: function(response){
                        console.log(response);
                    }
                });
            });

            $("#btnChangeElements").on("click",function(){
                var elements = $("#txtAmountOfElements").val();
                if(isNumber(elements)){
                    $.ajax({
                        url: "/set-elements",
                        type: "POST",
                        data: { elements: elements },
                        success: function(response){
                            $(".ResultChangeElements").html("<strong>Changed successfully</strong>");
                            $(".ResultChangeElements").show();
                            setTimeout(function() {
                                  $(".ResultChangeElements").hide(1000);
                            }, 3000);
                        },
                        error: function(response){
                            console.log(response);
                        }
                    });
                }
                else {
                    $(".ResultChangeElements").html("<strong>Only numbers are allowed</strong>");
                    $(".ResultChangeElements").show();
                    setTimeout(function() {
                          $(".ResultChangeElements").fadeOut(1000);
                    }, 3000);
                }
            });

            function reloadPage(){
                document.location.reload(true);
            }
            function isNumber(num) {
              return ! isNaN (num-0) && num !== null && num !== "" && num !== false && num !== " ";
            }
        });
