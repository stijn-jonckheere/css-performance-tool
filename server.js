// Server init
var express = require("express");
var cookieParser = require('cookie-parser');
var session = require('express-session');
var app = express();

// File system init
var path = require("path");

// Body van de post kunnen parsen
var bodyParser = require('body-parser');
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

// View engine init
var engine = require("ejs-locals");
app.engine("ejs",engine);
app.set("views",path.join(__dirname,"views"));
app.set("view engine","ejs");

// Add root folder
app.use(express.static(__dirname + '/'));

// Session init
app.use(cookieParser());
app.use(session({
  secret: 'css perf tool',
  resave: false,
  saveUninitialized: false,
  cookie: { httpOnly: false, maxAge: 36000000000 }
}));

// MySQL init
var mysql = require("mysql");

var pool = mysql.createPool({
    connectionLimit : 100, //important
    host     : 'us-cdbr-iron-east-02.cleardb.net',
    user     : 'b485efb7351beb',
    password : '4c9d9f22',
    database : 'heroku_9574ba69a498e52',
    debug    :  false
});

function setElements(request, response, value){
  if(value > 10000) { value = 10000; }
  request.session.element = value;
  request.session.save();
}

function getElements(request, response){
  return (request.session.element) ? request.session.element : 500;
}

function setCssOption(request, response, value){
  request.session.cssOption = value;
  request.session.save();
}
function getCssOption(request, response){
  return (request.session.cssOption) ? request.session.cssOption : "none";
}

function setStyleTag(request, response, value){
  request.session.styleTag = value;
  request.session.save();
}
function getStyleTag(request, response){
  return (request.session.styleTag) ? request.session.styleTag : "ids";
}

// Routeconfig
app.get("/", function(request,response){
  response.render("index", { title: "test" });
});


/**
 * Set the amount of elements to use during the test
 */
app.post("/set-elements",function(request,response){
  setElements(request, response, request.body["elements"]);
  response.send(true);
});

/**
 * Set the way CSS should be loaded
 */
app.post("/set-css-option",function(request,response){
  setCssOption(request, response, request.body["cssOption"]);
  response.send(true);
});

/**
 * Set the way CSS should be loaded
 */
app.post("/set-style-tag",function(request,response){
  setStyleTag(request, response, request.body["styleTag"]);
  response.send(true);
});

/**
 * Clear the database results for the applying-performance-test
 */
app.post("/clear-applied-performance-result",function(request,response){
  pool.getConnection(function(err,connection){
    connection.query("DELETE FROM appliedperformance");
    connection.release();
    response.send(true);
  });
});
/**
 * Clear the database results for the loading-performance-test
 */
app.post("/clear-loading-performance-result",function(request,response){
  pool.getConnection(function(err,connection){
    connection.query("DELETE FROM loadingperformance");
    connection.release();
    response.send(true);
  });
});



/**
 * Load the second test & get the results from the database
 */
app.get("/applied-performance", function(request,response){
  pool.getConnection(function(err,connection){
    connection.query("SELECT CONCAT(browser,' v',VERSION) AS browserinfo, speed AS loadingspeed, elements, styleTag AS selector, transferSpeed, DATE_FORMAT(createdAt, '%W %M %Y') AS date, DATE_FORMAT(createdAt, '%r') AS time FROM appliedperformance ORDER BY createdAt DESC LIMIT 10",
      function(error,result){
        connection.release();
        response.render("applied-performance", { results: result, amountOfElements: getElements(request, response), styleTag: getStyleTag(request, response) });
    });
  });
});

/**
 * Run the second test
 */
app.get("/run-applied-performance-test", function(request,response){
  response.render("_applied-performance-test",{amountOfElements: getElements(request, response), styleTag: getStyleTag(request, response)});
});

/**
 * Add new measuring result to the database for test 2 and refresh the page
 */
app.post("/post-selector-performance-result",function(request,response){
  var result = request.body;
  var now = new Date();
  pool.getConnection(function(err,connection){
    connection.query("INSERT INTO appliedperformance(browser,version,speed,elements,styleTag,transferSpeed,createdAt) VALUES(?,?,?,?,?,?,?)",
      [request.body.browserName, request.body.browserVersion, request.body.loadingSpeed, getElements(request, response), getStyleTag(request, response), request.body.transferSpeed, now], function(error,result){
      connection.release();
      response.send(true);
    });
  });
});


app.get("/loading-performance", function(request,response){
  pool.getConnection(function(err,connection){
    connection.query("SELECT CONCAT(browser,' v',VERSION) AS browserinfo, speed AS loadingspeed, elements, cssOption AS cssoption, transferSpeed, DATE_FORMAT(createdAt, '%W %M %Y') AS date, DATE_FORMAT(createdAt, '%r') AS time FROM loadingperformance ORDER BY createdAt DESC LIMIT 10",
      function(error,result){
      connection.release();
      response.render("loading-performance", { results: result, amountOfElements: getElements(request, response), cssOption: getCssOption(request, response) });
    });
  });
});

/**
 * Run the second test
 */
app.get("/run-loading-performance-test", function(request,response){
  response.render("_loading-performance-test",{amountOfElements: getElements(request, response), cssOption: getCssOption(request, response)});
});


/**
 * Add new measuring result to the database and refresh the page
 */
app.post("/post-performance-result",function(request,response){
  var now = new Date();
  pool.getConnection(function(err,connection){
    connection.query("INSERT INTO loadingperformance(browser,version,speed,elements,cssOption,transferSpeed, createdAt) VALUES(?,?,?,?,?,?,?)",
      [request.body.browserName, request.body.browserVersion, request.body.loadingSpeed, getElements(request, response), getCssOption(request, response), request.body.transferSpeed, now], function(error,result){
        connection.release();
        response.send(true);
    });
  });
});

/**
 * Get all the chart data and generate the results page
 */
app.get("/performance-results",function(request,response){
  var browserSpeedArray = [["browser","Speed (ms)"]];
  var browserSpeedArray2 = [["browser","Speed (ms)"]];
  var loadingSpeedArray = [["Option","Speed (ms)"]];
  var appliedSpeedArray = [["Option","Speed (ms)"]];

  pool.getConnection(function(err,connection){
    connection.query("SELECT browser, ROUND(AVG(speed)) AS 'speed' FROM loadingperformance GROUP BY browser",
      function(error,result){
        connection.release();
        for(var i=0; i<result.length; i++){
          browserSpeedArray.push([result[i].browser, result[i].speed]);
        }
    });
  });

  pool.getConnection(function(err,connection){
    connection.query("SELECT browser, ROUND(AVG(speed)) AS 'speed' FROM appliedperformance GROUP BY browser",
      function(error,result){
      connection.release();
      for(var i=0; i<result.length; i++){
        browserSpeedArray2.push([result[i].browser, result[i].speed]);
      }
    });
  });

  pool.getConnection(function(err,connection){
    connection.query("SELECT cssOption, ROUND(AVG(speed)) AS 'speed' FROM loadingperformance GROUP BY cssOption",
      function(error,result){
      connection.release();
      for(var i=0; i<result.length; i++){
        loadingSpeedArray.push([result[i].cssOption, result[i].speed]);
      }
    });
  });

  pool.getConnection(function(err,connection){
    connection.query("SELECT styleTag, ROUND(AVG(speed)) AS 'speed' FROM appliedperformance GROUP BY styleTag",
      function(error,result){
      connection.release();
      for(var i=0; i<result.length; i++){
        appliedSpeedArray.push([result[i].styleTag, result[i].speed]);
      }
      response.render("performance-results", {
        browserSpeedArray: browserSpeedArray,
        browserSpeedArray2: browserSpeedArray2,
        loadingSpeedArray: loadingSpeedArray,
        appliedSpeedArray: appliedSpeedArray });
    });
  });
});

// Server start
var server = app.listen(process.env.PORT || 3000);
