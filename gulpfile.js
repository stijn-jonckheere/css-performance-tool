var gulp   = require('gulp'),
   gutil = require('gulp-util'),
   jshint = require('gulp-jshint'),
   sass   = require('gulp-sass'),
   sourcemaps = require('gulp-sourcemaps'),
   concat = require('gulp-concat'),
   minifyCSS = require('gulp-minify-css'),
   uglify = require('gulp-uglify');

gulp.task('default', ['watch']);

gulp.task('jshint', function() {
 return gulp.src('source/js/**/*.js')
   .pipe(jshint())
   .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('build-css', function() {
 return gulp.src('source/scss/**/*.scss')
   .pipe(sourcemaps.init())  // Process the original sources
     .pipe(sass( { errLogToConsole: true } ))
     //only minify if gulp is ran with '--type production'
     .pipe(gutil.env.type === 'production' ? minifyCSS() : gutil.noop())
     // add the map to modified source only if gulp is ran with '--type production'
     .pipe(gutil.env.type !== 'production' ? sourcemaps.write() : gutil.noop())
   .pipe(gulp.dest('css'));
});

gulp.task('build-js', function() {
 return gulp.src('source/js/**/*.js')
   .pipe(sourcemaps.init())
     .pipe(concat('bundle.js'))
     //only uglify if gulp is ran with '--type production'
     .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop())
     // add the map to modified source only if gulp is ran without '--type production'
     .pipe(gutil.env.type !== 'production' ? sourcemaps.write() : gutil.noop())
   .pipe(gulp.dest('js'));
});

gulp.task('watch', function() {
 gulp.watch('source/scss/**/*.scss', ['build-css']);
 gulp.watch('source/js/**/*.js', ['jshint']);
 gulp.watch('source/js/**/*.js', ['build-js']);
});
